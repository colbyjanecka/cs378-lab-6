// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CS378_Lab6GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB6_API ACS378_Lab6GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
