// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab6AI.h"
#include "Lab6AIController.h"

// Sets default values
ALab6AI::ALab6AI()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AIControllerClass = ALab6AIController::StaticClass();

}

// Called when the game starts or when spawned
void ALab6AI::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALab6AI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ALab6AI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

