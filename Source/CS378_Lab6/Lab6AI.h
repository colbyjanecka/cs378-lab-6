// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BehaviorTree.h"
#include "GameFramework/Character.h"
#include "Lab6AI.generated.h"

UCLASS()
class CS378_LAB6_API ALab6AI : public ACharacter
{
	GENERATED_BODY()
	

public:
	// Sets default values for this character's properties
	ALab6AI();

	FORCEINLINE class UBehaviorTree* GetBehaviorTree() const { return BehaviorTree;; }
	FORCEINLINE TArray<AActor*> GetPatrolPoints() const { return PatrolPoints; }
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAcess = "true"))
		UBehaviorTree * BehaviorTree;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAcess = "true"))
		TArray<AActor*>  PatrolPoints;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
