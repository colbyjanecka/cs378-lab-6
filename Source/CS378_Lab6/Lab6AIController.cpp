// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab6AIController.h"

#include "Lab6AI.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Kismet/GameplayStatics.h"
#include "PatrolPoint.h"

ALab6AIController::ALab6AIController()
{
	CurrentPatrolPoint = 0;
	NextPointKey = "NextPoint";
	
	BehaviorTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorTreeComponent"));
	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComponent"));
}

void ALab6AIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	// Get Reference to the character
	ALab6AI* AICharacter = Cast<ALab6AI>(InPawn);
	
	/**
	BlackboardComponent->InitializeBlackboard(*(AICharacter->BehaviorTree->BlackboardAsset));

	// if the cast was successfully
	if (AICharacter)
	{
		if (AICharacter->BehaviorTree->BlackboardAsset)
		{
			BlackboardComponent->InitializeBlackboard(*(AICharacter->BehaviorTree->BlackboardAsset));
		}

		// Populate patrol point array
		//UGameplayStatics::GetAllActorsOfClass(GetWorld(), APatrolPoint::StaticClass(), PatrolPoints);
		PatrolPoints = AICharacter->GetPatrolPoints();
		BlackboardComponent->SetValueAsObject(NextPointKey, PatrolPoints[CurrentPatrolPoint]);

		// run the behavior tree
		BehaviorTreeComponent->StartTree(*AICharacter->BehaviorTree);
	}
	*/
	if(BlackboardComponent)
	{
		if(AICharacter && AICharacter->BehaviorTree)
		{
			if(AICharacter->BehaviorTree->BlackboardAsset)
				BlackboardComponent->InitializeBlackboard(*(AICharacter->BehaviorTree->BlackboardAsset));

			PatrolPoints = AICharacter->GetPatrolPoints();
			BlackboardComponent->SetValueAsObject(NextPointKey, PatrolPoints[CurrentPatrolPoint]);

			if(BehaviorTreeComponent)
			{
				BehaviorTreeComponent->StartTree(*AICharacter->BehaviorTree);
				GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Red, FString::Printf(TEXT("Tree Started!")));
			}
				
		}
	}
}

