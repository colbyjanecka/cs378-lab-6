// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "Lab6AIController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB6_API ALab6AIController : public AAIController
{
	GENERATED_BODY()
	
	
public:
	ALab6AIController();

	FORCEINLINE class UBlackboardComponent* GetBlackboardComponent() const { return BlackboardComponent; }
	FORCEINLINE class UBehaviorTreeComponent* GetBehaviorTreeComponent() const { return BehaviorTreeComponent; }
	FORCEINLINE int32 GetCurrentPatrolPoint() const { return CurrentPatrolPoint; }
	FORCEINLINE void SetCurrentPatrolPoint(int32 NewPatrolPoint) { CurrentPatrolPoint = NewPatrolPoint; }
	FORCEINLINE TArray<AActor*> GetPatrolPoints() const { return PatrolPoints; }
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAcess = "true"))
		class UBehaviorTreeComponent * BehaviorTreeComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAcess = "true"))
		class UBlackboardComponent * BlackboardComponent;

	// Control point stuff
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAcess = "true"))
		int32 CurrentPatrolPoint;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAcess = "true"))
		TArray<AActor*>  PatrolPoints;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAcess = "true"));
		FName NextPointKey;

protected:
	virtual void OnPossess(APawn* InPawn) override;	

};
