// Fill out your copyright notice in the Description page of Project Settings.


#include "PatrolTaskNode.h"
#include "Lab6AIController.h"
#include "PatrolPoint.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UPatrolTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory)
{
	ALab6AIController * AIController = Cast<ALab6AIController>(OwnerComponent.GetAIOwner());

	if(AIController)
	{
		UBlackboardComponent * BlackboardComponent = AIController->GetBlackboardComponent();

		TArray<AActor*> AvailablePatrolPoints = AIController->GetPatrolPoints();
		int32 CurrentPatrolPoint = AIController->GetCurrentPatrolPoint();

		APatrolPoint* NextPoint = Cast<APatrolPoint>(AvailablePatrolPoints[0]);


		if(CurrentPatrolPoint >= AvailablePatrolPoints.Num() - 1)
		{
			AIController->SetCurrentPatrolPoint(0);
		}
		else
		{
			NextPoint = Cast<APatrolPoint>(AvailablePatrolPoints[++CurrentPatrolPoint]);
			AIController->SetCurrentPatrolPoint((CurrentPatrolPoint));
		}

		BlackboardComponent->SetValueAsObject(AIController->NextPointKey, NextPoint);

		return EBTNodeResult::Succeeded;
		
	}
	
	return EBTNodeResult::Failed;
}
