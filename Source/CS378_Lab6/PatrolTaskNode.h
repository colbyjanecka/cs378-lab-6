// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "PatrolTaskNode.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB6_API UPatrolTaskNode : public UBTTaskNode
{
	GENERATED_BODY()

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent & OwnerComponent, uint8 * NodeMemory) override;
	
};
